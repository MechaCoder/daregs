# DaRegs

![App Logo](./assets/home.jpg)

`DaRegs` is An Android App, and a project and aimed at helping or the `normannis folk` with useful functionality that helps people, Here you can from your android you can not only promptly access the regs. but you can also access fresh functionality to help you build your skill

+ `Society Structure`; A Quick handy glif, to help you understand where you are in the world of normannis.
+ `Authentic Orders`; here you can all the orders you need to shout in battle.
+ `Colour Chart`; An interactive method of finding your next colour scheme with the ability to share.
+ `Nice Links`; some really useful links for people this is a handy quick way, of accessing information that you need in a pinch, E.G. the local weatherSpoons, or where the nearest service station is.

## who has controbuted

+ Matthew Proudman

## Installing App
This app is not currently on the play store and because of this, you will need to go through a few more steps. it is very different depending on the Version of Android you running, I recommend that you read this [article](https://www.wikihow.tech/Install-APK-Files-on-Android#Installing_an_APK_File_from_the_File_Manager_sub).

## Legal Stuff

__while my this app will be free at the point of use it dose use propairty Infomain that is owned By someone. I do not wish any offence to the creater of the content, and infrigement of copyright is not intented.__

## Project Information

the App is built as useing React Native and is currently tested on Android, as of the 2/12/18 the app has not been built for IOS. this is due to the apple license restrictions. the App has been built useing the `expo` to asist whit the devlopment and build process, I am also useing ESlint to asist with the code,

### Set up for dev

1. Clone useing `git` the code base on to an area.
2. `npm install` to install the dependecys required to run the keychain.
3. `expo start` to start the dev server and attach your device though the expo app.

when commiting change sets i like to use `git flow` pleses use Pull Reqests, so i can review code before letting it go live

|command|what it dose|
|---|---|
|`expo start`|this starts [expo's](https://expo.io/) devlopment server |
|`exp build:android` | this conducts a APK build.`|

## But i just want the App

You can download `.apk` file and or the codebase from [here](https://bitbucket.org/MechaCoder/daregs/downloads/), [The APK](https://bitbucket.org/MechaCoder/daregs/downloads/DaRegs-37580abd8c0a42629212ce672550486c-signed(1).apk)

## Tested Devices

these are the devices that the app is know to work on,

|Android Version| Notes |
|---|---|
|android 8.0.0| this is the orgrinal dev device |
