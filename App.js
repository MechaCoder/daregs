import React from 'react'
import { StyleSheet, Text, View, ScrollView, Dimensions, Image } from 'react-native'
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome'
import { createStackNavigator, createAppContainer } from 'react-navigation'

import Header from './componets/header'
import BallatoresPage from './componets/screens/bellatores'
import LaboratoresPage from './componets/screens/laboratores'
import OratoresPage from './componets/screens/oratores'
import ResourcesPage from './componets/screens/resources'

class App extends React.Component {
  static navigationOptions = {

    title: 'Home'
  }

  render () {
    let widthValue = Dimensions.get('window').width / 4

    let btnStyle = {
      width: widthValue,
      margin: -15
    }

    return (
      <View style={styles.container}>
        <Header />
        <ScrollView style={styles.scrollview}>
          <Image source={require('./assets/home.jpg')} />
          <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 36 }}> New Stuff </Text>
          <Text style={{ marginBottom: 20, textAlign: 'justify', fontSize: 20, fontStyle: 'italic' }}> There have been some minnor changes and fixing some spelling errors, The very 'Shiny new thing is a color matching tool that can be found in the reousrces page.' </Text>
        </ScrollView>
        <View style={{ flex: 0, flexDirection: 'row', height: 25, width: Dimensions.get('window').width }}>
          <Button
            // title=""
            onPress={() => this.props.navigation.navigate('Ballatores')}
            backgroundColor="#f92727"

            icon={{
              name: 'change-history',
              size: 15,
              color: 'white'
            }}

            buttonStyle={btnStyle}

          />
          <Button
            // title=""
            onPress={() => this.props.navigation.navigate('Laboratores')}
            backgroundColor="#9999ff"
            icon={{
              name: 'build',
              size: 15,
              color: 'white'
            }}

            buttonStyle={btnStyle}

          />
          <Button
            // title=""
            onPress={() => this.props.navigation.navigate('Oratores')}
            backgroundColor="purple"
            icon={{
              name: 'school',
              size: 15,
              color: 'white'
            }}

            buttonStyle={btnStyle}

          />
          <Button
            // title=""
            onPress={() => this.props.navigation.navigate('Resources')}
            icon={{
              name: 'attach-file',
              size: 15,
              color: 'white'
            }}

            buttonStyle={btnStyle}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-evenly'
  },
  scrollview: {
    backgroundColor: 'pink',
    width: Dimensions.get('window').width,
    height: 80 // (Dimensions.get('window').height - 80)
  }
})

const navig = createStackNavigator({
  Home: {
    screen: App
  },
  Ballatores: {
    screen: BallatoresPage
  },
  Laboratores: {
    screen: LaboratoresPage
  },
  Oratores: {
    screen: OratoresPage
  },
  Resources: {
    screen: ResourcesPage
  }
})

export default createAppContainer(navig)
// export default navig;
