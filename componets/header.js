/* eslint-disable no-unused-vars */
import React from 'react'
import { StyleSheet, Text, View, Dimensions } from 'react-native'

export default class Header extends React.Component {
  render () {
    return (
      <View style={headerStyles.view} >
        <Text style={headerStyles.text} >DaRegs</Text>
      </View>
    )
  }
}

const headerStyles = StyleSheet.create({
  view: {
    backgroundColor: 'brown',
    width: Dimensions.get('window').width,
    paddingTop: 20,
    paddingBottom: 10
  },
  text: {
    color: '#fff',
    fontSize: 32
  }
})
