import React from 'react'
import { ScrollView, View, Image, Dimensions } from 'react-native'
import RegFile from '../object/regs'

export default class LaboratoresPage extends React.Component {
    /* eslint-disable-next-line */
    static navigationOptions = {
      title: 'Laboratores'
    }

    constructor () {
      super()

      this.state = {
        files: [
          {
            name: 'Laboratores – Civilian Regulations',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2017/03/Laboratores-Civilian-Regulations-v1.1.pdf'
          },
          {
            name: 'High Steward’s Laboratores-Handbook',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2018/01/High-Stewards-Laboratores-Handbook-1.0b.pdf'
          },
          {
            name: 'Clothing Regulations',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2017/03/Clothing-Regulations-1.2b.pdf'
          },
          {
            name: 'Tentage and Encampment Regulations',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2014/01/Tentage-and-Encampment-Regulations-v1.0.pdf'
          },
          {
            name: 'Justiciar’s Officer Handbook',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2014/01/Justiciars-Officer-Handbook-2.0.pdf'
          },
          {
            name: 'Scottish Clothing Regulations',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2017/02/ScottishClothing1.1.pdf'
          },
          {
            name: 'Scottish Field Manual',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2017/02/ScottishFieldManual1.0.pdf'
          },
          {
            name: 'Welsh Clothing Regulations',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2017/03/WelshClothing1.0.pdf'
          },
          {
            name: 'Welsh Field Manual',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2017/02/WelshFieldManual1.0.pdf'
          },
          {
            name: 'Guild of Drapers – Tablet Weaving Guide',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2016/11/Tablet-Weaving-Guide-25-2-2016.pdf'
          }
        ]
      }
    }

    render () {
      let files = []

      for (var i = 0; i <= this.state.files.length; i++) {
        var row = this.state.files[i]

        if (row === undefined) {
          continue
        }

        files.push(
          <RegFile
            key={[i]}
            fileName={row.name}
            filePath={row.file}
            btnColor="blue"
          />
        )
      }

      return (
        <ScrollView style={{ backgroundColor: '#b2b2ff' }}>
          <View style={{ alignItems: 'center', width: Dimensions.get('window').width, marginTop: 25 }}>
            <Image style={{ width: 100, height: 100 }} source={require('../../assets/icons8-worker-24.png')} />
          </View>
          <View>{files}</View>
        </ScrollView>
      )
    }
}
