import React from 'react'
import { View, ScrollView } from 'react-native'

import SocietyStructure from '../object/refs/societyStructure'
import ColorThing from '../object/refs/colorThemeer'
import UsefulLinks from '../object/refs/useful_links'
import Orders from '../object/refs/orders'

export default class ResourcesPage extends React.Component {
    static navigationOptions = {
      title: 'Resources'
    }

    render () {
      return (
        <ScrollView>
          <SocietyStructure />
          <Orders />
          <ColorThing />
          <UsefulLinks />
        </ScrollView>
      )
    }
}
