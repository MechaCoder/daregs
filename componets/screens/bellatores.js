/* eslint-disable no-unused-vars */
import React from 'react'
import { ScrollView, View, Image, Dimensions } from 'react-native'

import RegFile from './../object/regs'

export default class BallatoresPage extends React.Component {
    /* eslint-disable-next-line */
    static navigationOptions = {
      title: 'Bellatores'
    }

    constructor () {
      super()

      this.state = {
        files: [
          {
            name: 'WarGear Regs',
            file: 'https://normannis.co.uk/staticFiles/Wargear-Regulations-2.2.pdf'
          },
          {
            name: 'High Constable Constabulary Handbook',
            file: 'https://normannis.co.uk/staticFiles/Constabulary%20Handbook%202.2.pdf'
          },
          {
            name: 'High Constable Combat and Field Manual',
            file: 'https://normannis.co.uk/staticFiles/Combat%20and%20Field%20Manual%201.2.pdf' // this url needs changeing
          },
          {
            name: 'Motte & Shot – Fortification and Engine Regulations',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2018/05/Motte-and-Shot-1.1.pdf'
          },
          {
            name: 'Scottish Wargear',
            file: 'https://normannis.co.uk/staticFiles/ScottishWargear2.2.pdf'
          },
          {
            name: 'Welsh Wargear',
            file: 'https://normannis.co.uk/staticFiles/Welsh-Wargear-1.2.pdf'
          },
          {
            name: 'Byzantine Military Regulations',
            file: 'https://normannis.co.uk/staticFiles/DV%20Byzantine%202.1.pdf'
          }
        ]
      }
    }

    render () {
      let files = []

      for (var i = 0; i <= this.state.files.length; i++) {
        var row = this.state.files[i]

        if (row === undefined) {
          continue
        }

        files.push(<RegFile
          key={i}
          fileName={row.name}
          filePath={row.file}
          btnColor="#f92727" // https://www.color-hex.com/color/f92727
        />)
      }

      return (
        <ScrollView
          style={{
            backgroundColor: '#ff9999'
          }} // https://www.color-hex.com/color/ff9999
        >
          <View style={ { alignItems: 'center', width: Dimensions.get('window').width, marginTop: 25 } }>
            <Image style={ { width: 100, height: 100 } } source={require('../../assets/icons8-knight-24.png') } />
          </View>
          <View>{files}</View>
        </ScrollView>
      )
    }
}
