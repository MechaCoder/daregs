import React from 'react'
import { ScrollView, View, Image, Dimensions } from 'react-native'
import RegFile from '../object/regs'

export default class OratoresPage extends React.Component {
    /* eslint-disable-next-line */
    static navigationOptions = {
      title: 'Oratores'
    }

    constructor () {
      super()

      this.state = {
        files: [
          {
            name: 'Ecclesiastic & Holy Order Regulations',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2014/11/Ecclesiastic-Holy-Order-Regulations-v1.0.pdf'
          },
          {
            name: 'Lord Chancellor’s Oratores Handbook',
            file: 'https://normannis.co.uk/wp/wp-content/uploads/2014/01/Lord-Chancellors-Oratores-Handbook-1.01.pdf'
          },
          {
            name: 'DeusVult Holy Orders',
            file: 'https://normannis.co.uk/staticFiles/DV-HolyOrders%20v1.0.pdf'
          }
        ]
      }
    }

    render () {
      let files = []

      for (var i = 0; i <= this.state.files.length; i++) {
        var row = this.state.files[i]

        if (row === undefined) {
          continue
        }

        files.push(
          <RegFile
            key={[i]}
            fileName={row.name}
            filePath={row.file}
            btnColor="purple"
          />
        )
      }

      return (
        <ScrollView style={{ backgroundColor: '#bf7fbf' }}>
          <View style={{ alignItems: 'center', width: Dimensions.get('window').width, marginTop: 25 }}>
            <Image style={{ width: 100, height: 100 }} source={require('../../assets/icons8-cathedral-24.png')} />
          </View>
          <View>{files}</View>
        </ScrollView>
      )
    }
}
