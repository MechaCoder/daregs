/* eslint-disable no-unused-vars */
import React from 'react'
import { View, ScrollView, Dimensions, Picker, Share } from 'react-native'
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome'

import RefBase from './refenceBase'

class ColourPicker extends React.Component {
  constructor () {
    super()

    this.state = {
      type: 'wool',
      selectedColor: '',
      closed: true
    }

    this.getColors = this.getColors.bind(this)
  }

  getColors () {
    let colors = this.props.woolColors
    if (this.props.type === 'linien') {
      colors = this.props.linenColors
    }
    return colors
  }

  render () {
    let colors = this.getColors()

    let pickerItem = []
    for (var i = 0; i <= colors.length; i++) {
      if (colors[i] === undefined) {
        continue
      }

      if (colors[i].index === undefined) {
        continue
      }

      if (colors[i].hex === undefined) {
        continue
      }

      pickerItem.push(
        <Picker.Item key={i} value={colors[i].hex} label={colors[i].index} />
      )
    }

    return (
      <View style={{ backgroundColor: this.state.selectedColor, marginBottom: 20 }}>
        <View>
          <Picker selectedValue={this.state.type} onValueChange={(itemValue, itemIndex) => { this.setState({ type: itemValue }) }} >
            <Picker.Item label="Wool" value="wool" />
            <Picker.Item label="linien" value="linien" />
          </Picker>
          <Picker selectedValue={this.state.selectedColor} onValueChange={(itemValue, itemIndex) => { this.setState({ selectedColor: itemValue }); this.props.updateColor(this.props.keyid, itemValue) }} >
            {pickerItem}
          </Picker>
        </View>
      </View>
    )
  }
}

export default class ColorThing extends RefBase {
  constructor (props) {
    super()
    this.state = {
      els: 1,
      activeTier: 1,
      closed: true,
      sharedColors: {
        0: 'A1',
        1: 'A1'
      }
    }

    let colors = {
      A1: '#F4AF85',
      A2: '#ED701C',
      A3: '#9B2501',
      B1: '#FFBEBA',
      B2: '#FF8F8E',
      B3: '#FD4C4E',
      B4: '#FE0000',
      B5: '#C70102',
      B6: '#7A120B',
      C1: '#FFF0CD',
      C2: '#FFDA70',
      C3: '#FFC000',
      C4: '#C28E03',
      D1: '#DEA37E',
      D2: '#9E612B',
      D3: '#714321',
      D4: '#392318',
      E1: '#C6E0B3',
      E2: '#92D14F',
      E3: '#35AA42',
      E4: '#008001',
      E5: '#015000',
      F1: '#D0CECF',
      F2: '#AEAAA9',
      F3: '#757170',
      F4: '#3A3839',
      F5: '#000000',
      G1: '#DCEBF4',
      G2: '#9AC2E6',
      G3: '#5B9CD5',
      G4: '#2F74B4',
      G5: '#204E79',
      G6: '#233565',
      H1: '#02B0F3',
      H2: '#0166FE',
      H3: '#0001FE',
      H4: '#020099',
      I1: '#EBDBF5',
      I2: '#CDABE7',
      I3: '#A770CD',
      I4: '#7030A0',
      I5: '#441D60'
    }

    this.state.rawColors = colors

    this.state.linenTiers = {
      1: [colors.A1, colors.C1],
      2: [
        colors.A1, colors.B1, colors.C1,
        colors.D1, colors.E1, colors.F1,
        colors.G1
      ],
      3: [
        colors.A1, colors.B1, colors.C1,
        colors.D1, colors.E1, colors.F1,
        colors.G1, colors.C2
      ],
      4: [
        colors.A1, colors.B1, colors.C1,
        colors.D1, colors.E1, colors.F1,
        colors.G1, colors.A2, colors.B2,
        colors.C2, colors.D2, colors.E2,
        colors.F2, colors.G2
      ],
      5: [
        colors.A1, colors.B1, colors.C1,
        colors.D1, colors.E1, colors.F1,
        colors.H1,
        colors.G1, colors.A2, colors.B2,
        colors.C2, colors.D2, colors.E2,
        colors.F2, colors.G2
      ],
      6: [
        colors.G1, colors.A2, colors.B2,
        colors.C2, colors.D2, colors.E2,
        colors.F2, colors.G2, colors.H2,
        colors.B3, colors.C3, colors.D3,
        colors.E3, colors.F3, colors.G3
      ],
      7: [
        colors.A1, colors.A2, colors.A3,
        colors.B4, colors.C4, colors.D4,
        colors.E4, colors.F4, colors.G4,
        colors.H4, colors.I2
      ],
      8: [],
      9: []

    }

    this.state.woolTiers = {
      1: [
        colors.A1, colors.B1, colors.C2,
        colors.D1, colors.D2, colors.D3,
        colors.D4, colors.E1, colors.F2,
        colors.G2
      ],
      2: [
        colors.A2, colors.B2, colors.C2,
        colors.D1, colors.D2, colors.D3,
        colors.D4, colors.E1, colors.F2,
        colors.G2
      ],
      3: [
        colors.A2, colors.B2, colors.C3,
        colors.D1, colors.D2, colors.D3,
        colors.D4, colors.E2, colors.F2,
        colors.G3, colors.H1
      ],
      4: [
        colors.A2, colors.B2, colors.C3,
        colors.D1, colors.D2, colors.D3,
        colors.D4, colors.E2, colors.F2,
        colors.G3, colors.H1
      ],
      5: [
        colors.A3, colors.B4, colors.C4,
        colors.D1, colors.D2, colors.D3,
        colors.D4, colors.E3, colors.F3,
        colors.G4, colors.H2, colors.I1
      ],
      6: [
        colors.A3, colors.B5, colors.C4,
        colors.D1, colors.D2, colors.D3,
        colors.D4, colors.E4, colors.F4,
        colors.G6, colors.H4, colors.I1
      ],
      7: [
        colors.A3, colors.B6, colors.C4,
        colors.D1, colors.D2, colors.D3,
        colors.D4, colors.E4, colors.F4,
        colors.G6, colors.H4, colors.I2
      ],
      8: [],
      9: []
    }

    for (var key in colors) { // process tear 8, 9
      this.state.linenTiers[9].push(colors[key])
      this.state.woolTiers[9].push(colors[key])
      if (key === 'I5') {
        continue
      }

      this.state.linenTiers[8].push(colors[key])
      this.state.woolTiers[8].push(colors[key])
    }

    this._addEl = this._addEl.bind(this)
    this._remEl = this._remEl.bind(this)
    this.updateColors = this.updateColors.bind(this)
  }

  updateColors (index, color) {
    const thing = this.state.sharedColors

    var colorName = this._getName(color)

    if (colorName === undefined) {
      colorName = 'A1'
    }
    thing[index] = colorName

    this.setState({ 'sharedColors': thing })
  }

  _addEl () {
    this.updateColors(
      (this.state.els + 1),
      'F4AF85'
    )

    console.log('update')

    this.setState({
      els: (this.state.els + 1)
    })
  }

  _remEl () {
    if (this.state.els.length === 1) {
      return true
    }

    var shareslist = this.state.sharedColors
    delete shareslist[this.state.els]

    this.setState({ els: (this.state.els - 1) })
  }

  _getName (hex) {
    for (var key in this.state.rawColors) {
      if (this.state.rawColors[key] === hex) {
        return key
      }
    }
  }

  _getNamedObj (list) {
    // this takes a array and returns a list of colors with there index value.

    let rObj = []
    for (var i = 0; i <= list.length; i++) {
      rObj.push({
        'index': this._getName(list[i]),
        'hex': list[i]
      })
    }

    return rObj
  }

  render () {
    let wool = this._getNamedObj(
      this.state.woolTiers[this.state.activeTier]
    )

    let linen = this._getNamedObj(
      this.state.linenTiers[this.state.activeTier]
    )

    let ColourPickerArray = []
    for (var i = 0; i <= this.state.els; i++) {
      ColourPickerArray.push(
        <ColourPicker
          key={i}
          keyid={i}
          woolColors={wool}
          linenColors={linen}
          updateColor={this.updateColors}
        />
      )
    }

    let btnWidth = 90

    return (
      <View style={{ paddingBottom: 10, paddingTop: 10 }}>
        {this._renderHideButton('Colour Picker')}
        <View style={{ display: this._getSateElem().hideValue }}>
          <View style={{ backgroundColor: 'lightgrey' }}>
            <Picker
              style={{ width: Dimensions.get('window').width / 2, flexDirection: 'column' }}
              selectedValue={this.state.activeTier}
              onValueChange={(itemValue, itemIndex) => { this.setState({ activeTier: itemValue }) } }
            >
              <Picker.Item label="1" value="1" />
              <Picker.Item label="2" value="2" />
              <Picker.Item label="3" value="3" />
              <Picker.Item label="4" value="4" />
              <Picker.Item label="5" value="5" />
              <Picker.Item label="6" value="6" />
              <Picker.Item label="7" value="7" />
              <Picker.Item label="8" value="8" />
              <Picker.Item label="8" value="8" />
              <Picker.Item label="9" value="9" />
            </Picker>

            <View style={{
              flexDirection: 'row',
              width: Dimensions.get('window').width,
              alignItems: 'center',
              marginBottom: 5
            }}>
              <Button
                title="Add"
                onPress={this._addEl}
                color="white"
                backgroundColor="green"
                buttonStyle={{
                  width: btnWidth
                }}
              />

              <Button
                title="Remove"
                onPress={this._remEl}
                color="white"
                backgroundColor="red"
                buttonStyle={{
                  width: btnWidth,
                  flexDirection: 'column-reverse'
                }}
              />

              <Button
                title="Share"
                onPress={() => {
                  console.log(this.state.sharedColors)
                  const colorList = Object.values(this.state.sharedColors)

                  var message = ' A Color Scheme that at tier' + this.state.activeTier + '; ' + colorList.join(',')
                  Share.share({
                    title: 'A color scheme',
                    message: message
                  })
                }}
                color="white"
                backgroundColor="black"
                buttonStyle={{
                  width: btnWidth
                }}
              />
            </View>
          </View>
          <ScrollView style={{
            height: 400,
            width: Dimensions.get('window').width,
            borderColor: 'black',
            borderWidth: 1
          }} >
            {ColourPickerArray}
          </ScrollView>
        </View>
      </View>
    )
  }
}
