import React from 'react'
import { View, Image, Dimensions, Text, Picker } from 'react-native'

import refenceBase from './refenceBase'

class Table extends React.Component {
  constructor () {
    super()

    this.state = {
      'languge': 'anglo'
    }
  }

  mkcell (content, widthInt) {
    if (typeof content !== 'string') {
      console.warn('the content string is need')
      return false
    }

    if (typeof widthInt !== 'number') {
      console.warn('the width should be a number')
      return false
    }

    let styleSheet = {
      flexDirection: 'column',
      borderBottomWidth: 0.25
    }

    if (widthInt !== undefined) {
      styleSheet['width'] = widthInt
    }

    return (<View style={styleSheet}><Text>{content}</Text></View>)
  }

  mkRow (row) {
    let rowObjs = []
    let widthInt = [105, Dimensions.get('window').width / row.length]
    for (let i = 0; i <= row.length; i++) {
      if (typeof row[i] !== 'string') {
        continue
      }

      rowObjs.push(this.mkcell(row[i], widthInt[i]))
    }

    return (<View style={{ flexDirection: 'row' }}>
      {rowObjs}
    </View>)
  }

  mkTable (lingo) {
    var listIndex = [0, 2]
    if (lingo === 'french') {
      listIndex = [0, 1]
    }

    let orderValue = [
      ['Form', 'Formez! (for-may)/ A Forme! (Ahfihrm)', 'Awicken! (ah-wicken!)'],
      ['Present', 'Presente les armes! (prezont lez arms!)', 'Rearath spar! (rearroth spar!)'],
      ['Advance', 'Avancez! (a-vonsay!) Avant! (ahh-vont!)', 'Gegen forth (gay-gen forth!)'],
      ['Fall Back', 'Retout! (Rehtoo!)/Donnez-sol(Donn-nay sol)', 'Withertrod! (withertrod!)'],
      ['Charge', 'Chargez! (Shar-jay)', 'Onraes! (On-rass!)'],
      ['Halt', 'Aret! (A-reht)', 'Stedefast! (Stay-d fasst!)'],
      ['Turn', 'Tournez ... (turney)', 'Wendeth! (Wendethh!)'],
      ['(to the) Left', 'A gauche (a goesh)', 'Leoft! (leef-offt)'],
      ['(to the ) Right', 'A droit (a dwat)', 'Richte! (rick-tuh)'],
      ['Quickly', 'Vite! (Veet!)', 'Fleete! (Fleet-uh!)'],
      ['Dress', 'Dresse (Dreh-say!)', 'Trimmen! (Trimenn!)'],
      ['(Form) Line', '(Formez) rangee!(For-may ron-jay!)', 'Gurth raew ficliath! (Ger-th raw fik-leeath!)'],
      ['(Form) Column', '(Formez) Eschele!(For-may eh-shell!)', 'Sparfylka! (Sparr-fillkah!)'],
      ['(Form) Shieldwall', 'En embrasse-deescuz(On am-brass duh ehskoo)', 'Scealdborg! (skee-aldborg!)'],
      ['Company', 'Battaille! (Bah-tie)', 'Fylka! (Fill-ka)'],
      ['1st, 2nd, 3rd', 'Premiere,Deuxieme,Troisieme(Premm-yare, Duhzeeem, Twa-zee-em)', 'Firste, Twain,Drehain(Fur-stuh, Tway-ne, Drayen)'],
      ['Shields! (up/down)', 'Escuz!(ascende/descende!)(Eh-skoo ahsonnday/dess-sonn-day)', 'Schilde! (rearath/nameth)(Shill-duh rearathh/nahmethh)'],
      ['Archers (forward)', 'Arcier (avant) (Ahr-key-eh Ahvont)', 'Bowmen (forth) (Bohmen fawth)'],
      ['Nock', 'Preparez-tir! (Preh-parr-ray teer!)', 'Nockken Strael! (knockern Strahl!)'],
      ['Draw(/Aim)', 'Marquer!(Marhkur!)', 'Rearat[h Strael! (rearath Strahl!)'],
      ['Loose', 'Tirez! (Teer-eh!)', 'Straele Fugen! (strahl-uh f-yoogehn!)'],
      ['Surrender', 'Rendez! (Ron-day)', 'Crave! (Kray-ve)'],
      ['I surrender', 'Je me rends! (Juh muhrond)', 'Craven! (Kray-ven)']

    ]

    var returnList = []

    for (var i = 0; i <= orderValue.length; i++) {
      if (orderValue[i] === undefined) {
        continue
      }
      var temp = orderValue[i]
      returnList.push(
        [
          temp[ listIndex[0] ],
          temp[ listIndex[1] ]
        ]
      )
    }
    return returnList
  }

  render () {
    let rows = []
    let rawTable = this.mkTable(this.state.languge)
    for (var i = 0; i <= rawTable.length; i++) {
      if (rawTable[i] === undefined) {
        continue
      }
      rows.push(this.mkRow(rawTable[i]))
    }

    return (<View style={{ backgroundColor: 'pink', width: Dimensions.get('window').width - 30 }} >
      <Picker selectedValue={this.state.languge} onValueChange={(itemValue, itemIndex) => { this.setState({ 'languge': itemValue }) }} >
        <Picker.Item label="Anglo-Saxion" value="anglo" />
        <Picker.Item label="Norman French" value="french" />
      </Picker>
      <View>
        {rows}
      </View>
    </View>)
  }
}

export default class Orders extends refenceBase {
  render () {
    return (
      <View style={{ paddingLeft: 5, paddingBottom: 10, paddingTop: 10 }}>
        {this._renderHideButton('Authentic Orders')}
        <View style={{ display: this._getSateElem().hideValue, alignItems: 'center', paddingTop: 10 }}>
          <Table />
        </View>
      </View>
    )
  }
}
