/* eslint-disable no-unused-vars */
import React from 'react'
import { View, Dimensions, Linking, alert } from 'react-native'
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome'
import RefBase from './refenceBase'

export default class UsefulLinks extends RefBase {
  constructor () {
    super()

    this.state = {
      links: [
        {
          title: 'WeatherSpoons',
          url: 'https://www.google.com/maps/search/wetherspoons/'
        },
        {
          title: 'Wieland Forge',
          url: 'http://www.wielandforge.co.uk/'
        },
        {
          title: 'Wolfram Bows',
          url: 'https://www.facebook.com/Wolfram.Bows/'
        },
        {
          title: 'service stations',
          url: 'https://www.google.com/maps/search/service+stations/'
        }
      ],
      closed: true
    }
  }

  render () {
    var btnsList = []
    for (var i = 0; i <= this.state.links.length; i++) {
      if (this.state.links[i] === undefined) {
        continue
      }
      btnsList.push(<LinkingBtn key={i} title={this.state.links[i].title} url={this.state.links[i].url} />)
    }

    return (
      <View style={{ paddingLeft: 5, paddingBottom: 10, paddingTop: 10 }}>
        {this._renderHideButton('Nice Links')}
        <View style={{ display: this._getSateElem().hideValue, alignItems: 'center', paddingTop: 10 }}>
          {btnsList}
        </View>
      </View>
    )
  }
}

class LinkingBtn extends React.Component {
  constructor () {
    super()

    this.state = {}
    this._onPress = this._onPress.bind(this)
  }

  _onPress () {
    Linking.canOpenURL(this.props.url)
      .then(support => {
        if (!support) {
          console.log('Sorry can not do that')
          alert.alert("I can't do that sorry")
          return
        }
        return Linking.openURL(this.props.url)
      })
  }

  render () {
    return (
      <View style={{ paddingTop: 5, paddingBottom: 5, width: (Dimensions.get('window').width - 25) }}>
        <Button
          title={this.props.title}
          onPress={this._onPress}
          backgroundColor="blue"
        />
      </View>
    )
  }
}
