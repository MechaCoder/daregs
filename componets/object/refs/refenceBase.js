/* eslint-disable no-unused-vars */
import React from 'react'
import { Text, View } from 'react-native'
import { Button } from 'react-native-elements'

export default class RefBase extends React.Component {
  constructor () {
    super()

    this.state = {
      closed: true
    }

    this._onPress = this._onPress.bind(this)
    this._renderHideButton = this._renderHideButton.bind(this)
    this._getSateElem = this._getSateElem.bind(this)
  }

  _onPress () {
    this.setState({ closed: !this.state.closed })
  }

  _getSateElem () {
    let hideValue = 'flex'
    let arrowName = 'arrow-downward'
    if (this.state.closed) {
      hideValue = 'none'
      arrowName = 'arrow-forward'
    }

    return { 'hideValue': hideValue, 'arrowName': arrowName }
  }

  _renderHideButton (btnName) {
    let hideEl = this._getSateElem()

    return (
      <View>
        <Button
          title={btnName}
          onPress={this._onPress}
          color='white'
          backgroundColor='red'
          icon={{
            name: hideEl.arrowName,
            size: 15,
            color: 'white'
          }}
        />
      </View>
    )
  }

  render () {
    return (
      <View>
        <Text>This is a Thing</Text>
      </View>
    )
  }
}
