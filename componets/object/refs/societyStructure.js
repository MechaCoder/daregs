/* eslint-disable no-unused-vars */
import React from 'react'
import { View, ScrollView, Dimensions, Image } from 'react-native'
import { Button } from 'react-native-elements'
import RefBase from './refenceBase'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class SocietyStructure extends RefBase {
  // eslint-disable-next-line no-useless-constructor
  constructor () {
    super()
  }

  render () {
    let hideValue = 'flex'
    let arrowName = 'arrow-downward'
    if (this.state.closed) {
      hideValue = 'none'
      arrowName = 'arrow-forward'
    }

    return (<View style={{ paddingLeft: 5, paddingBottom: 10 }} >
      <View>
        <Button
          title="Society Structure"
          onPress={this._onPress}
          color='white'
          backgroundColor='red'

          icon={{
            name: arrowName,
            size: 15,
            color: 'white'
          }}
        />
      </View>
      <ScrollView style={{ display: hideValue }}>
        <Image
          source={require('../../../assets/hi_rotated.png')}
          resizeMode='stretch'
          style={{
            width: Dimensions.get('window').width - 10,
            height: 500
          }}
        />
      </ScrollView>
    </View>)
  }
}
