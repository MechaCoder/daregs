/* eslint-disable no-unused-vars */
import React from 'react'
import { Text, View, Image, Button, Linking, alert } from 'react-native'

export default class RegFile extends React.Component {
  constructor () {
    super()

    this._onPress = this._onPress.bind(this)
  }

  _onPress () {
    Linking.canOpenURL(this.props.filePath).then(support => {
      if (!support) {
        console.log('Sorry can not do that')
        alert.alert("I can't do that sorry")
        return
      }

      return Linking.openURL(this.props.filePath)
    })
  }

  render () {
    return (
      <View style={{ flex: 1, flexDirection: 'row', paddingTop: 20 }} >
        <Image style={{ height: 100, width: 100 }} source={require('../../assets/icons8-document-64.png')} />
        <View>
          <Text
            style={{
              marginTop: 10,
              width: 240,
              height: 50,
              textAlignVertical: 'center',
              textAlign: 'center'
            }}
          >
            {this.props.fileName}
          </Text>
          <Button
            title="Download File"
            onPress={this._onPress}
            color={this.props.btnColor}
          />
        </View>
      </View>
    )
  }
}
